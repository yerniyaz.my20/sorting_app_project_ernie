package org.SortingApp;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

public class AppTest {


    @org.junit.Test
    public void testZeroElementsArrayCase() {
        int[] array = {};
        App.sort(array);
        int[] arrayEmpty = {};
        assertArrayEquals(arrayEmpty, array);
    }

    @org.junit.Test
    public void testOneElementArrayCase() {
        int[] array = {13};
        App.sort(array);
        int[] arrayEmpty = {13};
        assertArrayEquals(arrayEmpty, array);
    }

    @org.junit.Test
    public void testTenElementsArrayCase() {
        int[] array = {1, 5, 10, 15, 13, 27, 3, 7, 17, 2};
        int[] arraySorted = {1, 2, 3, 5, 7, 10, 13, 15, 17, 27};
        App.sort(array);
        assertArrayEquals(arraySorted, array);
    }
    @org.junit.Test
    public void testMoreThanTenElementsArrayCase() {
        int[] array = {1, 5, 10, 15, 13, 27, 3, 7, 17, 2, 4, 9};
        int[] arraySorted = {1, 2, 3, 4, 5, 7, 9, 10, 13, 15, 17, 27};
        App.sort(array);
        assertArrayEquals(arraySorted, array);
    }

}