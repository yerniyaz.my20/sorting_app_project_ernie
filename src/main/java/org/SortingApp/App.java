package org.SortingApp;

import java.util.Arrays;
import java.util.Scanner;

//Complete the task to get a practical grasp of Apache Maven and its major features.
//You will need to create a Maven-based project – Sorting App.
//It is a small Java application that takes up to ten command-line arguments as integer values,
//sorts them in the ascending order, and then prints them into standard output.

public class App {
    public static void sort(int[] array) {
        Arrays.sort(array);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter up to 10 integer values to be sorted. Type '0' if you finished. ");
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
            if (array[i] == 0) {
                break;
            }
        }

        sort(array);

        System.out.println("Below are the integers you entered in ascending order: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                System.out.println(array[i]);
            }
        }
    }
}


